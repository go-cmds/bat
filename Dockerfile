FROM golang:1.8

ADD . /go/src/gitlab.com/go-cmds/gocurl

RUN go install gitlab.com/go-cmds/gocurl

ENTRYPOINT ["/go/bin/gocurl"]
